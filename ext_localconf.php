<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HIVE.HiveCptCntContactperson',
            'Hivecptcntcontactpersonrendercontactperson',
            [
                'Contactperson' => 'renderContactperson'
            ],
            // non-cacheable actions
            [
                'Contactperson' => '',
                'Contactsector' => ''
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HIVE.HiveCptCntContactperson',
            'Hivecptcntcontactpersonrendercontactsector',
            [
                'Contactsector' => 'renderContactsector'
            ],
            // non-cacheable actions
            [
                'Contactperson' => '',
                'Contactsector' => ''
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    hivecptcntcontactpersonrendercontactperson {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('hive_cpt_cnt_contactperson') . 'Resources/Public/Icons/user_plugin_hivecptcntcontactpersonrendercontactperson.svg
                        title = LLL:EXT:hive_cpt_cnt_contactperson/Resources/Private/Language/locallang_db.xlf:tx_hive_cpt_cnt_contactperson_domain_model_hivecptcntcontactpersonrendercontactperson
                        description = LLL:EXT:hive_cpt_cnt_contactperson/Resources/Private/Language/locallang_db.xlf:tx_hive_cpt_cnt_contactperson_domain_model_hivecptcntcontactpersonrendercontactperson.description
                        tt_content_defValues {
                            CType = list
                            list_type = hivecptcntcontactperson_hivecptcntcontactpersonrendercontactperson
                        }
                    }
                    hivecptcntcontactpersonrendercontactsector {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('hive_cpt_cnt_contactperson') . 'Resources/Public/Icons/user_plugin_hivecptcntcontactpersonrendercontactsector.svg
                        title = LLL:EXT:hive_cpt_cnt_contactperson/Resources/Private/Language/locallang_db.xlf:tx_hive_cpt_cnt_contactperson_domain_model_hivecptcntcontactpersonrendercontactsector
                        description = LLL:EXT:hive_cpt_cnt_contactperson/Resources/Private/Language/locallang_db.xlf:tx_hive_cpt_cnt_contactperson_domain_model_hivecptcntcontactpersonrendercontactsector.description
                        tt_content_defValues {
                            CType = list
                            list_type = hivecptcntcontactperson_hivecptcntcontactpersonrendercontactsector
                        }
                    }
                }
                show = *
            }
       }'
    );
    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

call_user_func(
    function($extKey)
    {

        // wizards
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
                wizards.newContentElement.wizardItems.plugins.elements.hivecptcntcontactpersonrendercontactperson >
                wizards.newContentElement.wizardItems {
                    hive {
                        header = Hive
                        after = common,special,menu,plugins,forms
                        elements.hivecptcntcontactpersonrendercontactperson {
                            iconIdentifier = hive_cpt_brand_32x32_svg
                            title = Contactperson
                            description = A list of Contactpersons
                            tt_content_defValues {
                                CType = list
                                list_type = hivecptcntcontactperson_hivecptcntcontactpersonrendercontactperson
                            }
                        }
                        show := addToList(hivecptcntcontactpersonrendercontactperson)
                    }

                },
                wizards.newContentElement.wizardItems.plugins.elements.hivecptcntcontactpersonrendercontactsector >
                wizards.newContentElement.wizardItems {
                    hive {
                        header = Hive
                        after = common,special,menu,plugins,forms
                        elements.hivecptcntcontactpersonrendercontactsector {
                            iconIdentifier = hive_cpt_brand_32x32_svg
                            title = Contactsector
                            description = A list of Contactpersons within sectors
                            tt_content_defValues {
                                CType = list
                                list_type = hivecptcntcontactperson_hivecptcntcontactpersonrendercontactsector
                            }
                        }
                        show := addToList(hivecptcntcontactpersonrendercontactsector)
                    }

                }
            }'
        );

    }, $_EXTKEY
);