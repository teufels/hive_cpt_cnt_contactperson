<?php
namespace HIVE\HiveCptCntContactperson\Domain\Model;

/***
 *
 * This file is part of the "hive_cpt_cnt_contactperson" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * Contactperson
 */
class Contactperson extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * backendTitle
     *
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate ("NotEmpty")
     */
    protected $backendTitle = '';

    /**
     * name
     *
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate ("NotEmpty")
     */
    protected $name = '';

    /**
     * image
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade remove
     */
    protected $image = null;

    /**
     * phone
     *
     * @var string
     */
    protected $phone = '';

    /**
     * mobile
     *
     * @var string
     */
    protected $mobile = '';

    /**
     * fax
     *
     * @var string
     */
    protected $fax = '';

    /**
     * email
     *
     * @var string
     * @TYPO3\CMS\Extbase\Annotation\Validate ("NotEmpty")
     */
    protected $email = '';

    /**
     * url
     *
     * @var string
     */
    protected $url = '';

    /**
     * additional
     *
     * @var string
     */
    protected $additional = '';

    /**
     * sector
     *
     * @var \HIVE\HiveCptCntContactperson\Domain\Model\Contactsector
     */
    protected $sector = null;

    /**
     * Returns the backendTitle
     *
     * @return string $backendTitle
     */
    public function getBackendTitle()
    {
        return $this->backendTitle;
    }

    /**
     * Sets the backendTitle
     *
     * @param string $backendTitle
     * @return void
     */
    public function setBackendTitle($backendTitle)
    {
        $this->backendTitle = $backendTitle;
    }

    /**
     * Returns the name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the name
     *
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Returns the image
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Sets the image
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     * @return void
     */
    public function setImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $image)
    {
        $this->image = $image;
    }

    /**
     * Returns the phone
     *
     * @return string $phone
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Sets the phone
     *
     * @param string $phone
     * @return void
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * Returns the fax
     *
     * @return string $fax
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Sets the fax
     *
     * @param string $fax
     * @return void
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    }

    /**
     * Returns the email
     *
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets the fax
     *
     * @param string $fax
     * @return void
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Returns the url
     *
     * @return string $url
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Sets the url
     *
     * @param string $url
     * @return void
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * Returns the sector
     *
     * @return \HIVE\HiveCptCntContactperson\Domain\Model\Contactsector $sector
     */
    public function getSector()
    {
        return $this->sector;
    }

    /**
     * Sets the sector
     *
     * @param \HIVE\HiveCptCntContactperson\Domain\Model\Contactsector $sector
     * @return void
     */
    public function setSector(\HIVE\HiveCptCntContactperson\Domain\Model\Contactsector $sector)
    {
        $this->sector = $sector;
    }

    /**
     * Returns the mobile
     *
     * @return string $mobile
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Sets the mobile
     *
     * @param string $mobile
     * @return void
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
    }

    /**
     * Returns the additional
     *
     * @return string $additional
     */
    public function getAdditional()
    {
        return $this->additional;
    }

    /**
     * Sets the additional
     *
     * @param string $additional
     * @return void
     */
    public function setAdditional($additional)
    {
        $this->additional = $additional;
    }
}
