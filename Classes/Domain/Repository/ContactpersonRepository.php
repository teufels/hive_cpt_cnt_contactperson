<?php
namespace HIVE\HiveCptCntContactperson\Domain\Repository;

/***
 *
 * This file is part of the "hive_cpt_cnt_contactperson" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * The repository for Contactpersons
 */
class ContactpersonRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    protected $defaultOrderings = array(
        'sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING,
        // ::ORDER_DESCENDING = Absteigende Sortierung
    );

    /**
     * findByUidListOrderByList
     *
     * @param string String containing uids
     * @return \HIVE\HiveCptCntContactperson\Domain\Model\Contactperson
     */
    public function findByUidListOrderByList($uidList)
    {
        $uidArray = explode(',', $uidList);
        $result = [];
        foreach ($uidArray as $uid) {
            $result[] = $this->findByUid($uid);
        }
        return $result;
    }

    /**
     * Find by Uid
     *
     * @param int $uid
     * @return Btn
     */
    public function findByUid($uid)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectSysLanguage(FALSE);
        $query->getQuerySettings()->setRespectStoragePage(FALSE);
        $query->matching($query->equals('uid', $uid));
        return $query->execute()->getFirst();
    }

    /**
     * Find by Sectorid
     *
     * @param int $uid
     * @return Btn
     */
    public function findBySectorId($uid)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectSysLanguage(FALSE);
        $query->getQuerySettings()->setRespectStoragePage(FALSE);
        $query->matching($query->equals('sector', $uid));
        return $query->execute();
    }
}
