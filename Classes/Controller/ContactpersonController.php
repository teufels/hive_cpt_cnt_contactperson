<?php
namespace HIVE\HiveCptCntContactperson\Controller;

/***
 *
 * This file is part of the "hive_cpt_cnt_contactperson" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * ContactpersonController
 */
class ContactpersonController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * contactpersonRepository
     *
     * @var \HIVE\HiveCptCntContactperson\Domain\Repository\ContactpersonRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $contactpersonRepository = null;

    /**
     * action renderContactperson
     *
     * @return void
     */
    public function renderContactpersonAction()
    {
        $aSettings = $this->settings;
        $oSelectedContactperson = $aSettings['oContactperson'];
        $oSelectedContactperson = $this->contactpersonRepository->findByUidListOrderByList($oSelectedContactperson);
        $this->view->assign('contactperson', $oSelectedContactperson);
        $this->view->assign('settings', $aSettings);
    }
}
