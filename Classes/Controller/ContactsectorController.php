<?php
namespace HIVE\HiveCptCntContactperson\Controller;

/***
 *
 * This file is part of the "hive_cpt_cnt_contactperson" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * ContactsectorController
 */
class ContactsectorController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * contactsectorRepository
     *
     * @var \HIVE\HiveCptCntContactperson\Domain\Repository\ContactsectorRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $contactsectorRepository = null;

    /**
     * contactpersonRepository
     *
     * @var \HIVE\HiveCptCntContactperson\Domain\Repository\ContactpersonRepository
     * @TYPO3\CMS\Extbase\Annotation\Inject
     */
    protected $contactpersonRepository = null;

    /**
     * action renderContactsector
     *
     * @return void
     */
    public function renderContactsectorAction()
    {

        $aSettings = $this->settings;
        $oSelectedContactsector = $aSettings['oContactsector'];
        $aSelectedContactsector = explode(',', $oSelectedContactsector);
        $aSector = [];
        $i = 0;
        foreach ($aSelectedContactsector as $sector) {
            $aSector[$i]['sector'] = $this->contactsectorRepository->findByUidListOrderByList($sector);
            $aSector[$i]['person'] = $this->contactpersonRepository->findBySectorId($sector);
            $i ++;
        }
        $this->view->assign('contactsector', $aSector);
        $this->view->assign('settings', $aSettings);

    }
}
