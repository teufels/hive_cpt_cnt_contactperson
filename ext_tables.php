<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'HIVE.HiveCptCntContactperson',
            'Hivecptcntcontactpersonrendercontactperson',
            'hive_cpt_cnt_contactperson :: renderContactperson'
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'HIVE.HiveCptCntContactperson',
            'Hivecptcntcontactpersonrendercontactsector',
            'hive_cpt_cnt_contactperson :: renderContactsector'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('hive_cpt_cnt_contactperson', 'Configuration/TypoScript', 'hive_cpt_cnt_contactperson');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hivecptcntcontactperson_domain_model_contactperson', 'EXT:hive_cpt_cnt_contactperson/Resources/Private/Language/locallang_csh_tx_hivecptcntcontactperson_domain_model_contactperson.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hivecptcntcontactperson_domain_model_contactperson');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hivecptcntcontactperson_domain_model_contactsector', 'EXT:hive_cpt_cnt_contactperson/Resources/Private/Language/locallang_csh_tx_hivecptcntcontactperson_domain_model_contactsector.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hivecptcntcontactperson_domain_model_contactsector');

    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
