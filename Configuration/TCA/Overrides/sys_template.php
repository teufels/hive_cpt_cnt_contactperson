<?php
defined('TYPO3_MODE') or die();

$extKey = 'hive_cpt_cnt_contactperson';
$title = '[HIVE] Contactperson';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($extKey, 'Configuration/TypoScript', $title);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hivecptcntcontactperson_domain_model_contactperson', 'EXT:hive_cpt_cnt_contactperson/Resources/Private/Language/locallang_csh_tx_hivecptcntcontactperson_domain_model_contactperson.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hivecptcntcontactperson_domain_model_contactperson');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hivecptcntcontactperson_domain_model_contactsector', 'EXT:hive_cpt_cnt_contactperson/Resources/Private/Language/locallang_csh_tx_hivecptcntcontactperson_domain_model_contactsector.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hivecptcntcontactperson_domain_model_contactsector');