<?php
defined('TYPO3_MODE') or die();

/***************
 * Plugin
 ***************/
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'HIVE.HiveCptCntContactperson',
    'Hivecptcntcontactpersonrendercontactperson',
    'hive_cpt_cnt_contactperson :: renderContactperson'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'HIVE.HiveCptCntContactperson',
    'Hivecptcntcontactpersonrendercontactsector',
    'hive_cpt_cnt_contactperson :: renderContactsector'
);

$extensionName = strtolower('hive_cpt_cnt_contactperson');

$pluginName = strtolower('Hivecptcntcontactpersonrendercontactperson');
$pluginSignature = str_replace('_', '', $extensionName) . '_' . str_replace('_', '', $pluginName);
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'layout,select_key,pages,recursive';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:'. $extensionName . '/Configuration/FlexForms/Config.xml');

$pluginName = strtolower('Hivecptcntcontactpersonrendercontactsector');
$pluginSignature = str_replace('_', '', $extensionName) . '_' . str_replace('_', '', $pluginName);
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'layout,select_key,pages,recursive';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:'. $extensionName . '/Configuration/FlexForms/ConfigSector.xml');