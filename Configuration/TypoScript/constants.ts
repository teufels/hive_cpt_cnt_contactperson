
plugin.tx_hivecptcntcontactperson_hivecptcntcontactpersonrendercontactperson {
    view {
        # cat=plugin.tx_hivecptcntcontactperson_hivecptcntcontactpersonrendercontactperson/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hive_cpt_cnt_contactperson/Resources/Private/Templates/
        # cat=plugin.tx_hivecptcntcontactperson_hivecptcntcontactpersonrendercontactperson/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hive_cpt_cnt_contactperson/Resources/Private/Partials/
        # cat=plugin.tx_hivecptcntcontactperson_hivecptcntcontactpersonrendercontactperson/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hive_cpt_cnt_contactperson/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hivecptcntcontactperson_hivecptcntcontactpersonrendercontactperson//a; type=string; label=Default storage PID
        storagePid =
    }
}

plugin.tx_hivecptcntcontactperson_hivecptcntcontactpersonrendercontactsector {
    view {
        # cat=plugin.tx_hivecptcntcontactperson_hivecptcntcontactpersonrendercontactsector/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hive_cpt_cnt_contactperson/Resources/Private/Templates/
        # cat=plugin.tx_hivecptcntcontactperson_hivecptcntcontactpersonrendercontactsector/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hive_cpt_cnt_contactperson/Resources/Private/Partials/
        # cat=plugin.tx_hivecptcntcontactperson_hivecptcntcontactpersonrendercontactsector/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hive_cpt_cnt_contactperson/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hivecptcntcontactperson_hivecptcntcontactpersonrendercontactsector//a; type=string; label=Default storage PID
        storagePid =
    }
}

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder