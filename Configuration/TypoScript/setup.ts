
plugin.tx_hivecptcntcontactperson_hivecptcntcontactpersonrendercontactperson {
    view {
        templateRootPaths.0 = EXT:hive_cpt_cnt_contactperson/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_hivecptcntcontactperson_hivecptcntcontactpersonrendercontactperson.view.templateRootPath}
        partialRootPaths.0 = EXT:hive_cpt_cnt_contactperson/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_hivecptcntcontactperson_hivecptcntcontactpersonrendercontactperson.view.partialRootPath}
        layoutRootPaths.0 = EXT:hive_cpt_cnt_contactperson/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_hivecptcntcontactperson_hivecptcntcontactpersonrendercontactperson.view.layoutRootPath}
    }
    persistence {
        storagePid = {$plugin.tx_hivecptcntcontactperson_hivecptcntcontactpersonrendercontactperson.persistence.storagePid}
        #recursive = 1
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
}

plugin.tx_hivecptcntcontactperson_hivecptcntcontactpersonrendercontactsector {
    view {
        templateRootPaths.0 = EXT:hive_cpt_cnt_contactperson/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_hivecptcntcontactperson_hivecptcntcontactpersonrendercontactsector.view.templateRootPath}
        partialRootPaths.0 = EXT:hive_cpt_cnt_contactperson/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_hivecptcntcontactperson_hivecptcntcontactpersonrendercontactsector.view.partialRootPath}
        layoutRootPaths.0 = EXT:hive_cpt_cnt_contactperson/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_hivecptcntcontactperson_hivecptcntcontactpersonrendercontactsector.view.layoutRootPath}
    }
    persistence {
        storagePid = {$plugin.tx_hivecptcntcontactperson_hivecptcntcontactpersonrendercontactsector.persistence.storagePid}
        #recursive = 1
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
}

# these classes are only used in auto-generated templates
plugin.tx_hivecptcntcontactperson._CSS_DEFAULT_STYLE (
    textarea.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    input.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    .tx-hive-cpt-cnt-contactperson table {
        border-collapse:separate;
        border-spacing:10px;
    }

    .tx-hive-cpt-cnt-contactperson table th {
        font-weight:bold;
    }

    .tx-hive-cpt-cnt-contactperson table td {
        vertical-align:top;
    }

    .typo3-messages .message-error {
        color:red;
    }

    .typo3-messages .message-ok {
        color:green;
    }
)

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder